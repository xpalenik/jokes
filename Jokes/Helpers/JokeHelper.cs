﻿using System;
using System.Collections.Generic;

namespace Jokes.Helpers {
    public static class JokeHelper {

        /// <summary> Splits a list into chunks of a certain size. </summary>
        /// <typeparam name="T">The type of an object in a list.</typeparam>
        /// <param name="list">Current list.</param>
        /// <param name="size">The size of a chunk.</param>
        /// <returns>A list of smaller lists.</returns>
        public static IEnumerable<List<T>> Split<T>(this List<T> list, int size)
        {
            var chunks = new List<List<T>>();
            for (int i = 0; i < list.Count; i += size)
                chunks.Add(list.GetRange(i, Math.Min(size, list.Count - i)));
            return chunks;
        } 
    }
}