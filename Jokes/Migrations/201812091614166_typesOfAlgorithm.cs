namespace Jokes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class typesOfAlgorithm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ratings", "RecommendedBy", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ratings", "RecommendedBy");
        }
    }
}
