namespace Jokes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AspNetUsers", new[] { "Voter_Id" });
            AlterColumn("dbo.AspNetUsers", "Voter_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "Voter_Id");
            DropColumn("dbo.AspNetUsers", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            DropIndex("dbo.AspNetUsers", new[] { "Voter_Id" });
            AlterColumn("dbo.AspNetUsers", "Voter_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Voter_Id");
        }
    }
}
