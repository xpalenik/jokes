﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jokes.DAL;

namespace Jokes.Models.Facades
{
    public class RatingsFacade
    {
        private readonly JokesDbContext db = new JokesDbContext();

        public void SaveRating(int jokeId, int voterId, float vote, int recommnededBy)
        {
            db.Ratings.Add(new Rating()
            {
                JokeId = jokeId,
                VoterId = voterId,
                Vote = vote,
                RecommendedBy = recommnededBy
            });
            db.SaveChanges();
        }
    }
}