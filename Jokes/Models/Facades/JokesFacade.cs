﻿using Jokes.DAL;
using Jokes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Core.Objects;

namespace Jokes.Models.Facades
{
    public class JokesFacade
    {
        private readonly JokesDbContext db = new JokesDbContext();
        private Random rand = new Random();

        public Joke GetJokeById(int id)
        {
            Joke joke = db.Jokes.Find(id) == null ? new Joke() : db.Jokes.Find(id);
            return joke;
        }

        public Joke GetJokeById(int id, string recommendedBy)
        {
            Joke joke = db.Jokes.Find(id) == null ? new Joke() : db.Jokes.Find(id);
            joke.RecommendedBy = recommendedBy;
            return joke;
        }

        public Joke GetAnotherRandomJoke(int previousJokeId)
        {
            if (GetJokeById(rand.Next(0, db.Jokes.Count())).Id != previousJokeId)
                return GetJokeById(rand.Next(1, db.Jokes.Count()), "1");
            else
                return GetAnotherRandomJoke(previousJokeId);
        }

        public Joke GetAnotherRecommendedJoke(int previousJokeId)
        {
            var allJokes = from joke in db.Jokes
                           select joke;

            List<Joke> listOfGoodJokes = new List<Joke>();
            List<Joke> listOfProbablyBadJokes = new List<Joke>();

            List<Joke> listOfAllJokes = allJokes.ToList();
            foreach (Joke joke in listOfAllJokes)
            {
                joke.RecommendedBy = "2";
                if (joke.Ratings == null || joke.Ratings.Count > 0)
                {
                    List<float> ratings = new List<float>();
                    foreach (Rating rate in joke.Ratings)
                        ratings.Add((float)rate.Vote);

                    if (ratings.Average() >= 3.0)
                        listOfGoodJokes.Add(joke);
                    else
                        listOfProbablyBadJokes.Add(joke);
                }
                else
                    listOfProbablyBadJokes.Add(joke);
            }

            if (rand.Next(1, 5) > 1)
                return listOfGoodJokes.ElementAt(rand.Next(listOfGoodJokes.Count()));
            else
                return listOfProbablyBadJokes[rand.Next(listOfProbablyBadJokes.Count())];
        }

        public Joke GetAnotherRecommendedJoke(int previousJokeId, string userName)
        {
            var currentUser = db.Users.Where(u => u.UserName == userName).First();

            if (currentUser.Voter.Ratings.Count < 10)
                return GetAnotherRecommendedJoke(previousJokeId);
            
            (List<Voter> neighbours, bool cosineSimilarityUsed) = getNearestNeighbours(currentUser.Voter, 10);
            List<int> idOfJokesSuggested = new List<int>();

            foreach (var neighbour in neighbours)
            {
                foreach (var rating in neighbour.Ratings)
                {
                    if (rating.Vote > 4.0)
                        idOfJokesSuggested.Add(rating.JokeId);
                }
            }

            if (cosineSimilarityUsed)
                return GetJokeById(idOfJokesSuggested[rand.Next(idOfJokesSuggested.Count)], "4");
            else
                return GetJokeById(idOfJokesSuggested[rand.Next(idOfJokesSuggested.Count)], "3");
        }

        private (List<Voter>,bool) getNearestNeighbours(Voter currentVoter, int numberOfNeighbours)
        {
            var otherVoters = db.Voters.Where(v => v.Id != currentVoter.Id).ToList();

            Dictionary<Voter, float> scoreOfNeighbours = new Dictionary<Voter, float>();
            bool cosineSimilarityUsed = false;
            
            if (rand.Next(0,2) == 1)
            {
                foreach (Voter voter in otherVoters)
                {
                    scoreOfNeighbours.Add(voter, Pearson(currentVoter, voter));
                }
            }
            else
            {
                cosineSimilarityUsed = true;
                foreach (Voter voter in otherVoters)
                {
                    scoreOfNeighbours.Add(voter, CosineSimilarity(currentVoter, voter));
                }
            }

            var sortedDict = (from entry in scoreOfNeighbours orderby entry.Value descending select entry)
               .ToDictionary(pair => pair.Key, pair => pair.Value).Take(numberOfNeighbours);

            List<Voter> listToReturn = new List<Voter>();
            foreach (var item in sortedDict)
                listToReturn.Add(item.Key);
            return (listToReturn, cosineSimilarityUsed);
        }

        private float Pearson(Voter currentVoter, Voter otherVoter)
        {
            List<Rating> ratingsOfCurrentUser = new List<Rating>();
            List<Rating> ratingsOfOtherUser = new List<Rating>();

            foreach (var rating in currentVoter.Ratings)
            {
                foreach (var ratingOfOtherUser in otherVoter.Ratings)
                {
                    if (rating.JokeId == ratingOfOtherUser.JokeId)
                    {
                        ratingsOfCurrentUser.Add(rating);
                        ratingsOfOtherUser.Add(ratingOfOtherUser);
                    }
                }
            }

            float sumOfCurrentUserRatings = 0;

            foreach (var currentUserRating in ratingsOfCurrentUser)
                sumOfCurrentUserRatings = sumOfCurrentUserRatings + (float)currentUserRating.Vote;

            float sumOfOtherUserRatings = 0;

            foreach (var otherUserRating in ratingsOfOtherUser)
                sumOfOtherUserRatings = sumOfOtherUserRatings + (float)otherUserRating.Vote;

            float averageCurrentVoter = sumOfCurrentUserRatings / ratingsOfCurrentUser.Count;
            float averageOfOtherVoter = sumOfOtherUserRatings / ratingsOfOtherUser.Count;

            float top = 0;
            for (int i = 0; i < ratingsOfCurrentUser.Count; i++)
                top = top + ((float)ratingsOfCurrentUser[i].Vote - averageCurrentVoter) * ((float)ratingsOfOtherUser[i].Vote - averageOfOtherVoter);

            float normalizedCurrent = 0;

            for (int i = 0; i < ratingsOfCurrentUser.Count; i++)
            {
                float nonNormalizedCurrent = (float)ratingsOfCurrentUser[i].Vote - averageCurrentVoter;
                normalizedCurrent = normalizedCurrent + (float)Math.Pow((double)nonNormalizedCurrent, 2);
            }

            normalizedCurrent = (float)Math.Sqrt((double)normalizedCurrent);

            float normalizedOther = 0;

            for (int i = 0; i < ratingsOfOtherUser.Count; i++)
            {
                float nonNormalizedOther = (float)ratingsOfOtherUser[i].Vote - averageOfOtherVoter;
                normalizedOther = normalizedOther + (float)Math.Pow((double)nonNormalizedOther, 2);
            }

            normalizedOther = (float)Math.Sqrt((double)normalizedOther);

            return top / (normalizedCurrent * normalizedOther);
        }

        private float CosineSimilarity(Voter currentVoter, Voter otherVoter)
        {
            List<Rating> ratingsOfCurrentUser = new List<Rating>();
            List<Rating> ratingsOfOtherUser = new List<Rating>();

            foreach (var rating in currentVoter.Ratings)
            {
                foreach (var ratingOfOtherUser in otherVoter.Ratings)
                {
                    if (rating.JokeId == ratingOfOtherUser.JokeId)
                    {
                        ratingsOfCurrentUser.Add(rating);
                        ratingsOfOtherUser.Add(ratingOfOtherUser);
                    }
                }
            }

            float top = 0;
            for (int i = 0; i < ratingsOfCurrentUser.Count; i++)
                top = top + ((float)ratingsOfCurrentUser[i].Vote) * ((float)ratingsOfOtherUser[i].Vote);

            double nonNormalizedVectorCurrentSize = 0;

            for (int i = 0; i < ratingsOfCurrentUser.Count; i++)
            {
                nonNormalizedVectorCurrentSize = nonNormalizedVectorCurrentSize + Math.Pow((double)ratingsOfCurrentUser[i].Vote, 2);
            }

            float normalizedVectorCurrentSize = (float)Math.Sqrt(nonNormalizedVectorCurrentSize);

            double nonNormalizedVectorOtherSize = 0;

            for (int i = 0; i < ratingsOfOtherUser.Count; i++)
            {
                nonNormalizedVectorOtherSize = nonNormalizedVectorOtherSize + Math.Pow((double)ratingsOfOtherUser[i].Vote, 2);
            }

            float normalizedVectorOtherSize = (float)Math.Sqrt(nonNormalizedVectorOtherSize);
            return (float)Math.Acos((double)(top / (normalizedVectorCurrentSize * normalizedVectorOtherSize)));
        }
    }
}