﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using Jokes.DAL;

namespace Jokes.Models
{
    /// <summary> The representation of a user voting on a <see cref="Joke"/>. </summary>
    /// <remarks> <see cref="ApplicationUser"/> is needlessly complex with too many
    /// required fields; so Jester database couldn't be easily loaded.</remarks>
    public class Voter
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        /// <summary> Link to the voter's user information and login credentials.</summary>
        /// <remarks> Not all voters need to be registered users (e.g. the Jester dataset). </remarks>
        public virtual ApplicationUser User { get; set; }

        /// <summary> This voter's ratings of various jokes. </summary>
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}