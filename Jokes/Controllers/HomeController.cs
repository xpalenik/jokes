﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jokes.Models;
using Jokes.Models.Facades;

using Newtonsoft.Json;
using System.IO;
using Jokes.DAL;

namespace Jokes.Controllers
{
    public class HomeController : Controller
    {
        private readonly RatingsFacade ratingFacade = new RatingsFacade();
        private readonly JokesFacade jokesFacade = new JokesFacade();
        private readonly JokesDbContext db = new JokesDbContext();

        public ActionResult Index(int id)
        {
            //Joke joke = new Joke();
            Joke joke = jokesFacade.GetAnotherRandomJoke(id);
            ViewBag.Content = joke.Content;
            joke.Content = joke.Id.ToString();
            joke.CurrentRating = 0;
            joke.RecommendedBy = "1";
            return View(joke);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ThanksForRating(Joke joke)
        {
            ViewBag.Message = "Thank you!";

            return View();
        }

        [HttpPost]
        public ActionResult VoteCast(Joke joke)
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
                return View("PleaseRegister", joke);

            int jokeId = int.Parse(joke.Content);
            var voterId = from voter in db.Voters
                          where voter.User.UserName == User.Identity.Name
                          select voter.Id;

            ratingFacade.SaveRating(jokeId, voterId.First(), joke.CurrentRating, int.Parse(joke.RecommendedBy));

            joke.Id = jokeId;

            return View("ThanksForRating", joke);
        }

        public ActionResult GetNewRandomJoke(int id)
        {
            Joke joke = jokesFacade.GetAnotherRandomJoke(id);
            ViewBag.Content = joke.Content;
            joke.Content = joke.Id.ToString();
            joke.CurrentRating = 0;
            joke.RecommendedBy = "1";

            return View("Index", joke);
        }

        public ActionResult GetNewRecommendedJoke(int id)
        {
            Joke joke;
            if (string.IsNullOrEmpty(User.Identity.Name))
                joke = jokesFacade.GetAnotherRecommendedJoke(id);
            else
                joke = jokesFacade.GetAnotherRecommendedJoke(id, User.Identity.Name);
            ViewBag.Content = joke.Content;
            joke.Content = joke.Id.ToString();
            joke.CurrentRating = 0;

            return View("Index", joke);
        }
    }
}