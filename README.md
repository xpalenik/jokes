# Table of Contents
1.  [Team members](#team-members)
2.  [Task](#task)
3.  [Assignment](#assignment)
4.  [Dataset](#dataset)
5.  [Basic workflow](#basic-workflow)
6.  [Technologies](#technologies)

## Team members
*  Jan Kužílek (422344)
*  Tomáš Ježek (422778)
*  Viktória Tibenská (475928)
*  Martin Páleník (359817) -- [commentary](https://gitlab.com/359817/jokes/blob/master/MPalenik/commentary_on_project.pdf), [dataset analysis](https://gitlab.com/359817/jokes/blob/master/MPalenik/analysis_of_dataset.html)

## Task
Joke Recommender System

## Assignment

*Source:* 
Course [homepage](https://www.fi.muni.cz/~xpelanek/PV254/)

> Goal: To build a simple recommender system.
> The focus should be on functionality (not on user interface). The system should include enough content and functionality to be "interesting".

*Source:* 
An e-mail from the teacher

> As I mentioned, this course is project-based -- you are expected to create and present a project, either a prototype of a simple recommender system or analysis (predictions, evaluations) of offline data from some  existing system. [...] Think about a recommender system that you would like to create or analyze. 

## Dataset
Jester dataset -- from [project Jester](http://eigentaste.berkeley.edu/about.html)
*  Datasets containing over 5 million anonymous joke ratings from 150k users are freely available for research use.
*  Dataset format described in detail [here](http://eigentaste.berkeley.edu/dataset/)
*  *To-do:* visualization of the ranking distribution

We are using [Dataset 2](http://eigentaste.berkeley.edu/dataset/jester_dataset_2.zip) in our project, which has the following format.

**jester_ratings.dat**: Each row is formatted as: 

**User ID**　　　　　**Item ID**　　　　　**Rating**
```
1		26		0.031
1		92		3.625
1		118		0.000
1		134		3.344
2		5		-9.688
2		7		9.938
2		8		9.531
2		13		9.938
```

**jester_items.dat**: The actual texts of the jokes. Maps item ID's (from ```jester_ratings.dat```) to jokes.

```
1:
<p>
A man visits the doctor. The doctor says, &quot;I have bad news for you. 
You have cancer and Alzheimer&#039;s disease&quot;.<br />
<br />
The man replies, &quot;Well, thank God I don&#039;t have cancer!&quot;
</p>
```

*  The ratings are real values ranging from {--10.00-} to {++10.00+}
*  Jokes {1, 2, 3, 4, 5, 6, 9, 10, 11, 12, 14, 20, 27, 31, 43, 51, 52, 61, 73, 80, 100, 116} have been removed (i.e. they are never displayed or rated)
* The jokes aren't put into categories

### Data distribution
From left to right
1.  Distribution of Ratings (-10.00 to +10.00)
2.  Average Joke Rating (-4 to 10)
3.  Number of Ratings per Joke (0 to 40000)
![alt text](https://raw.githubusercontent.com/junolee/joke-recommender/master/images/eda.png "source: https://github.com/junolee/joke-recommender")

## Basic workflow
*  Reasonably pick a subset of the dataset
*  Do some other preprocessing if needed
*  Visualise the dataset (see [here](https://apple.github.io/turicreate/docs/userguide/vis/))
*  Discuss algorithms suitable for the given dataset (see [here](https://apple.github.io/turicreate/docs/userguide/recommender/choosing-a-model.html))
    * [Item Similarity Recommender](https://apple.github.io/turicreate/docs/api/generated/turicreate.recommender.item_similarity_recommender.ItemSimilarityRecommender.html)
        * A model that ranks an item according to its similarity to other items observed for the user in question.
        * Given a similarity between two items, it scores an item for user using a weighted average of the user’s previous observations.
    * [Ranking Factorization Recommender]()
        * A RankingFactorizationRecommender learns latent factors for each user and item and uses them to rank recommended items according to the likelihood of observing those (user, item) pairs. This is commonly desired when performing collaborative filtering for implicit feedback datasets or datasets with explicit ratings for which ranking prediction is desired.
        * A multitude of options and metrics, very versatile
        * Commonly desired when performing collaborative filtering
        * Recommendations are based on known scores of users and items.
*  Create a model/models
*  Evaluate the model
*  Export it in [CoreML format](https://docs.microsoft.com/cs-cz/xamarin/ios/platform/introduction-to-ios11/coreml) (.mlmodel extension) (see [here](https://apple.github.io/turicreate/docs/userguide/recommender/coreml-deployment.html))
*  Use the given model in the Web Application

## Technologies

[Turi Create](https://github.com/apple/turicreate) -- an opensource Pythonic Machine Learning library
*  The [User Guide](https://apple.github.io/turicreate/docs/userguide/)
*  The [API Documentation](https://apple.github.io/turicreate/docs/api/)
*  The section on [Recommender systems](https://apple.github.io/turicreate/docs/userguide/recommender/)
*  ASP.NET (Microsoft Azure) -- for creating a Web Application
*  Alternatively [Machine Learning for .NET](https://github.com/dotnet/machinelearning) or [Machine Learning in Azure](https://docs.microsoft.com/en-us/azure/machine-learning/studio-module-reference/machine-learning-modules)